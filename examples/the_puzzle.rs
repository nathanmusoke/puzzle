extern crate puzzle;
use puzzle::*;

fn p1() -> Piece {
    Piece::new(
        Tab::Out(Shape::Spade),
        Tab::In(Shape::Heart),
        Tab::In(Shape::Diamond),
    )
}
fn p1_rot() -> Piece {
    Piece::new(
        Tab::In(Shape::Heart),
        Tab::In(Shape::Diamond),
        Tab::Out(Shape::Spade),
    )
}
fn p2() -> Piece {
    Piece::new(
        Tab::Out(Shape::Heart),
        Tab::In(Shape::Diamond),
        Tab::In(Shape::Club),
    )
}
fn p3() -> Piece {
    Piece::new(
        Tab::In(Shape::Spade),
        Tab::In(Shape::Heart),
        Tab::Out(Shape::Club),
    )
}
fn p4() -> Piece {
    Piece::new(
        Tab::In(Shape::Spade),
        Tab::Out(Shape::Heart),
        Tab::In(Shape::Club),
    )
}
fn p5() -> Piece {
    Piece::new(
        Tab::Out(Shape::Heart),
        Tab::Out(Shape::Diamond),
        Tab::In(Shape::Spade),
    )
}
fn p6() -> Piece {
    Piece::new(
        Tab::In(Shape::Heart),
        Tab::Out(Shape::Diamond),
        Tab::Out(Shape::Spade),
    )
}
fn p7() -> Piece {
    Piece::new(
        Tab::Out(Shape::Spade),
        Tab::In(Shape::Diamond),
        Tab::Out(Shape::Club),
    )
}
fn p8() -> Piece {
    Piece::new(
        Tab::Out(Shape::Spade),
        Tab::In(Shape::Diamond),
        Tab::In(Shape::Club),
    )
}
fn p9() -> Piece {
    Piece::new(
        Tab::In(Shape::Club),
        Tab::Out(Shape::Diamond),
        Tab::In(Shape::Spade),
    )
}
fn p10() -> Piece {
    Piece::new(
        Tab::In(Shape::Heart),
        Tab::Out(Shape::Diamond),
        Tab::Out(Shape::Heart),
    )
}
fn p11() -> Piece {
    Piece::new(
        Tab::In(Shape::Heart),
        Tab::Out(Shape::Club),
        Tab::Out(Shape::Spade),
    )
}
fn p12() -> Piece {
    Piece::new(
        Tab::In(Shape::Club),
        Tab::Out(Shape::Diamond),
        Tab::Out(Shape::Spade),
    )
}
fn p13() -> Piece {
    Piece::new(
        Tab::In(Shape::Heart),
        Tab::Out(Shape::Club),
        Tab::Out(Shape::Diamond),
    )
}
fn p14() -> Piece {
    Piece::new(
        Tab::In(Shape::Diamond),
        Tab::Out(Shape::Heart),
        Tab::Out(Shape::Heart),
    )
}
fn p15() -> Piece {
    Piece::new(
        Tab::In(Shape::Spade),
        Tab::In(Shape::Diamond),
        Tab::Out(Shape::Heart),
    )
}
fn p16() -> Piece {
    Piece::new(
        Tab::In(Shape::Heart),
        Tab::In(Shape::Spade),
        Tab::Out(Shape::Club),
    )
}


fn main() {
    let puzzle = vec![
        p1(),
        p2(),
        p3(),
        p4(),
        p5(),
        p6(),
        p7(),
        p8(),
        p9(),
        p10(),
        p11(),
        p12(),
        p13(),
        p14(),
        p15(),
        p16(),
    ];
    let soln = solve(puzzle).unwrap();
    println!("{:#?}", soln);
}
