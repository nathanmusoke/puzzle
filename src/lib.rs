// `error_chain!` can recurse deeply
#![recursion_limit = "1024"]

// Import the macro. Don't forget to add `error-chain` in your
// `Cargo.toml`!
#[macro_use]
extern crate error_chain;

// We'll put our errors in an `errors` module, and other modules in
// this crate will `use errors::*;` to get access to everything
// `error_chain!` creates.
mod errors {
    // Create the Error, ErrorKind, ResultExt, and Result types
    error_chain!{}
}

// This only gives access within this module. Make this `pub use errors::*;`
// instead if the types must be accessible from other modules (e.g., within
// a `links` section).
use errors::*;


#[derive(Copy, Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum Shape {
    Club,
    Diamond,
    Heart,
    Spade,
}

#[derive(Copy, Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum Tab {
    None,
    In(Shape),
    Out(Shape),
}

impl Default for Tab {
    fn default() -> Tab {
        Tab::None
    }
}

impl Tab {
    fn fit(a: Tab, b: Tab) -> bool {
        match (a, b) {
            (Tab::In(c), Tab::Out(d)) => c == d,
            (Tab::Out(c), Tab::In(d)) => c == d,
            (Tab::None, _) => true,
            (_, Tab::None) => true,
            _ => false,
        }
    }
}

#[derive(Copy, Clone)]
#[derive(PartialEq)]
#[derive(Default, Debug)]
pub struct Piece {
    tab1: Tab,
    tab2: Tab,
    tab3: Tab,
}

impl Piece {
    pub fn new(t1: Tab, t2: Tab, t3: Tab) -> Piece {
        Piece {
            tab1: t1,
            tab2: t2,
            tab3: t3,
        }
    }

    fn get_tab(self: &Piece, side: usize) -> Tab {
        match side {
            1 => self.tab1,
            2 => self.tab2,
            3 => self.tab3,
            _ => panic!("Impossible side requested"),
        }
    }

    // TODO: return a ref
    fn rot(&self) -> Piece {
        Piece {
            tab1: self.tab2,
            tab2: self.tab3,
            tab3: self.tab1,
        }
    }
}

#[derive(Debug)]
pub struct Coord {
    row: isize,
    col: isize,
}

/// Represent solutions as a vector of vectors of pieces
#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Default, Debug)]
pub struct PartialSolution(Vec<Vec<Piece>>);

impl PartialSolution {
    /// Empty solution with given number of rows
    fn empty(rows: usize) -> PartialSolution {
        let mut p: Self = Default::default();
        for i in 0..rows {
            p.0.push(vec![Default::default(); 2 * i + 1]);
        }
        p
    }

    fn get_piece(&self, c: &Coord) -> Piece {
        if c.col.abs() > c.row || c.row >= self.0.len() as isize || c.row < 0 {
            Default::default()
        } else {
            self.0[c.row as usize][(c.row + c.col) as usize]
        }
    }

    fn set_piece(&mut self, p: &Piece, c: &Coord) -> Result<&mut Self> {
        if c.col.abs() > c.row || c.row >= self.0.len() as isize || c.row < 0 {
            Err(
                format!("Tried to insert piece outside grid: {:?}", c).into(),
            )
        } else {
            self.0[c.row as usize][(c.row + c.col) as usize] = *p;
            Ok(self)
        }
    }

    /// Try to fit a piece into a partial solution
    fn place_piece(&self, p: &Piece, loc: Coord) -> Option<PartialSolution> {
        // Location already occupied
        if self.get_piece(&loc).tab1 != Tab::None {
            return None;
        };

        if Tab::fit(
            p.get_tab(1),
            self.get_piece(&Coord {
                row: loc.row + (-1 as isize).pow((loc.row + loc.col) as u32),
                ..loc
            }).get_tab(1),
        ) &&
            Tab::fit(
                p.get_tab(2),
                self.get_piece(&Coord {
                    col: loc.col - (-1 as isize).pow((loc.row + loc.col) as u32),
                    ..loc
                }).get_tab(2),
            ) &&
            Tab::fit(
                p.get_tab(3),
                self.get_piece(&Coord {
                    col: loc.col + (-1 as isize).pow((loc.row + loc.col) as u32),
                    ..loc
                }).get_tab(3),
            ) {
            let new: PartialSolution = self.to_owned().set_piece(p, &loc).unwrap().clone();
            Some(new)
        } else {
            None
        }
    }
}

/// # Examples
///
/// Solve puzzle where pieces don't have to be rotated
///
/// ```
/// use puzzle::*;
///
/// let puzzle = vec![
///                 Piece::new(
///                         Tab::Out(Shape::Spade),
///                         Tab::In(Shape::Heart),
///                         Tab::In(Shape::Diamond)
///                 ),
///                 Piece::new(
///                         Tab::In(Shape::Spade),
///                         Tab::In(Shape::Heart),
///                         Tab::Out(Shape::Club)
///                 ),
///                 Piece::new(
///                         Tab::Out(Shape::Heart),
///                         Tab::In(Shape::Diamond),
///                         Tab::In(Shape::Club)
///                 ),
///                 Piece::new(
///                         Tab::In(Shape::Spade),
///                         Tab::Out(Shape::Heart),
///                         Tab::In(Shape::Club)
///                 ),
///             ];
///
/// let soln = solve(puzzle).unwrap();
/// println!("{:#?}", soln);
/// ```
pub fn solve(pieces: Vec<Piece>) -> Result<PartialSolution> {

    let partial = PartialSolution::empty(check_triangular(pieces.len())?);

    recurse(partial, &pieces, 0)
}

fn recurse(
    partial: PartialSolution,
    pieces: &[Piece],
    num_placed: usize,
) -> Result<PartialSolution> {
    if pieces.len() == 0 {
        return Ok(partial);
    }

    println!(
        "Received soln with {} pieces placed: {:#?}",
        num_placed,
        partial,
    );
    println!("{} pieces remaining: {:#?}", pieces.len(), pieces,);


    for (i, p) in pieces.iter().enumerate() {
        for p in [p, &p.rot(), &p.rot().rot()].iter() {
            match partial.place_piece(p, ith_coord(num_placed)) {
                Some(partial_new) => match recurse(
                    partial_new,
                    &[&pieces[..i], &pieces[i + 1..]].concat(),
                    num_placed + 1,
                ) {
                    Ok(soln) => return Ok(soln),
                    _ => continue,
                },
                None => continue,
            };
        }
    }

    Err("No solution found".into())
}

/// Get coord at which to place i'th piece
///
/// This is a good place to start optimizations later.
fn ith_coord(i: usize) -> Coord {

    let row = (i as f32).sqrt().floor() as isize;
    let in_row: isize = i as isize - row * row;

    Coord {
        row: row,
        col: -row + in_row,
    }
}

fn check_triangular(n: usize) -> Result<(usize)> {
    let sqrt = (n as f32).sqrt() as usize;
    match (sqrt * sqrt).eq(&n) {
        true => Ok((sqrt)),
        false => Err("Non-square number of pieces".into()),
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn tab_fit_in_in() {
        use {Shape, Tab};
        assert!(!Tab::fit(Tab::In(Shape::Club), Tab::In(Shape::Club)));
        assert!(!Tab::fit(Tab::In(Shape::Club), Tab::In(Shape::Diamond)));
    }

    #[test]
    fn tab_fit_out_out() {
        use {Shape, Tab};
        assert!(!Tab::fit(Tab::Out(Shape::Heart), Tab::Out(Shape::Heart)));
        assert!(!Tab::fit(Tab::Out(Shape::Heart), Tab::Out(Shape::Spade)));
    }

    #[test]
    fn tab_fit_out_in() {
        use {Shape, Tab};
        assert!(Tab::fit(Tab::Out(Shape::Club), Tab::In(Shape::Club)));
        assert!(Tab::fit(Tab::In(Shape::Diamond), Tab::Out(Shape::Diamond)));
        assert!(!Tab::fit(Tab::Out(Shape::Club), Tab::In(Shape::Diamond)));
        assert!(!Tab::fit(Tab::In(Shape::Diamond), Tab::Out(Shape::Club)));
    }

    #[test]
    fn tab_fit_in_out() {
        use {Shape, Tab};
        assert!(Tab::fit(Tab::In(Shape::Club), Tab::Out(Shape::Club)));
        assert!(!Tab::fit(Tab::In(Shape::Club), Tab::Out(Shape::Diamond)));
    }

    #[test]
    fn tab_fit_none() {
        use {Shape, Tab};
        assert!(Tab::fit(Tab::In(Shape::Club), Tab::None));
        assert!(Tab::fit(Tab::Out(Shape::Spade), Tab::None));
        assert!(Tab::fit(Tab::None, Tab::None));
    }

    #[test]
    fn check_triangular_01() {
        use check_triangular;
        assert!(check_triangular(1).is_ok())
    }

    #[test]
    fn check_triangular_02() {
        use check_triangular;
        assert!(check_triangular(2).is_err())
    }

    #[test]
    fn check_triangular_03() {
        use check_triangular;
        assert!(check_triangular(3).is_err())
    }

    #[test]
    fn check_triangular_04() {
        use check_triangular;
        assert!(check_triangular(4).is_ok())
    }

    #[test]
    fn get_tab_from_piece() {
        use {Piece, Shape, Tab};

        let t1 = Tab::In(Shape::Club);
        let t2 = Tab::In(Shape::Diamond);
        let t3 = Tab::Out(Shape::Heart);

        let p = Piece {
            tab1: t1,
            tab2: t2,
            tab3: t3,
        };

        assert_eq!(t1, p.get_tab(1));
        assert_eq!(t2, p.get_tab(2));
        assert_eq!(t3, p.get_tab(3));
    }

    #[test]
    fn place_first() {
        use {Coord, PartialSolution, Piece, Shape, Tab};
        let partial = PartialSolution::empty(2);
        println!("{:#?}", partial);

        let p = Piece {
            tab1: Tab::In(Shape::Club),
            tab2: Tab::In(Shape::Club),
            tab3: Tab::In(Shape::Club),
        };

        let partial_new = partial.place_piece(&p, Coord { row: 0, col: 0 });

        println!("{:#?}", partial_new);

        assert!(partial_new.is_some());

        let partial_new = partial_new.unwrap();
        assert_eq!(partial_new.0[0][0], p);
        assert_eq!(
            partial_new,
            PartialSolution(vec![vec![p], vec![Default::default(); 3]])
        );
    }

    #[test]
    fn place_second_adjacent_possible() {
        use {Coord, PartialSolution, Piece, Shape, Tab};
        let mut partial = PartialSolution::empty(2);

        let p1 = Piece {
            tab1: Tab::In(Shape::Club),
            tab2: Tab::In(Shape::Club),
            tab3: Tab::In(Shape::Club),
        };
        let p2 = Piece {
            tab1: Tab::Out(Shape::Club),
            tab2: Tab::In(Shape::Club),
            tab3: Tab::In(Shape::Club),
        };

        partial = partial.place_piece(&p1, Coord { row: 0, col: 0 }).unwrap();
        println!("{:#?}", partial);

        let partial_new = partial.place_piece(&p2, Coord { row: 1, col: 0 });
        println!("{:#?}", partial_new);


        assert!(partial_new.is_some());

        let partial_new = partial_new.unwrap();
        assert_eq!(
            partial_new,
            PartialSolution(vec![
                vec![p1],
                vec![Default::default(), p2, Default::default()],
            ])
        );
    }

    #[test]
    fn place_second_adjacent_impossible_1() {
        use {Coord, PartialSolution, Piece, Shape, Tab};
        let mut partial = PartialSolution::empty(2);

        let p1 = Piece {
            tab1: Tab::In(Shape::Club),
            tab2: Tab::In(Shape::Club),
            tab3: Tab::In(Shape::Club),
        };
        let p2 = Piece {
            tab1: Tab::In(Shape::Club),
            tab2: Tab::In(Shape::Club),
            tab3: Tab::In(Shape::Club),
        };

        partial = partial.place_piece(&p1, Coord { row: 0, col: 0 }).unwrap();
        println!("{:#?}", partial);

        let partial_new = partial.place_piece(&p2, Coord { row: 1, col: 0 });
        println!("{:#?}", partial_new);

        assert!(partial_new.is_none());
    }

    #[test]
    fn place_second_adjacent_impossible_2() {
        use {Coord, PartialSolution, Piece, Shape, Tab};
        let mut partial = PartialSolution::empty(2);

        let p1 = Piece {
            tab1: Tab::In(Shape::Club),
            tab2: Tab::In(Shape::Club),
            tab3: Tab::In(Shape::Club),
        };
        let p2 = Piece {
            tab1: Tab::In(Shape::Club),
            tab2: Tab::Out(Shape::Club),
            tab3: Tab::In(Shape::Club),
        };

        partial = partial.place_piece(&p1, Coord { row: 0, col: 0 }).unwrap();
        println!("{:#?}", partial);

        let partial_new = partial.place_piece(&p2, Coord { row: 1, col: 0 });
        println!("{:#?}", partial_new);

        assert!(partial_new.is_none());
    }

    #[test]
    fn place_third_adjacent_possible() {
        use {Coord, PartialSolution, Piece, Shape, Tab};
        let mut partial = PartialSolution::empty(2);

        let p1 = Piece {
            tab1: Tab::In(Shape::Club),
            tab2: Tab::Out(Shape::Diamond),
            tab3: Tab::Out(Shape::Diamond),
        };
        let p2 = Piece {
            tab1: Tab::Out(Shape::Spade),
            tab2: Tab::Out(Shape::Spade),
            tab3: Tab::In(Shape::Club),
        };
        let p3 = Piece {
            tab1: Tab::Out(Shape::Club),
            tab2: Tab::In(Shape::Heart),
            tab3: Tab::Out(Shape::Club),
        };

        partial = partial.place_piece(&p1, Coord { row: 0, col: 0 }).unwrap();
        partial = partial.place_piece(&p2, Coord { row: 1, col: -1 }).unwrap();
        println!("{:#?}", partial);

        let partial_new = partial.place_piece(&p3, Coord { row: 1, col: 0 });
        println!("{:#?}", partial_new);

        assert!(partial_new.is_some());

        let partial_new = partial_new.unwrap();
        assert_eq!(
            partial_new,
            PartialSolution(vec![vec![p1], vec![p2, p3, Default::default()]])
        );
    }

    #[test]
    fn place_forth_possible() {
        use {Coord, PartialSolution, Piece, Shape, Tab};
        let mut partial = PartialSolution::empty(2);

        // Actual puzzle pieces
        let p1 = Piece {
            tab1: Tab::Out(Shape::Spade),
            tab2: Tab::In(Shape::Heart),
            tab3: Tab::In(Shape::Diamond),
        };
        let p2 = Piece {
            tab1: Tab::Out(Shape::Heart),
            tab2: Tab::In(Shape::Diamond),
            tab3: Tab::In(Shape::Club),
        };
        let p3 = Piece {
            tab1: Tab::In(Shape::Spade),
            tab2: Tab::In(Shape::Heart),
            tab3: Tab::Out(Shape::Club),
        };
        let p4 = Piece {
            tab1: Tab::In(Shape::Spade),
            tab2: Tab::Out(Shape::Heart),
            tab3: Tab::In(Shape::Club),
        };

        partial = partial.place_piece(&p1, Coord { row: 0, col: 0 }).unwrap();
        partial = partial.place_piece(&p2, Coord { row: 1, col: -1 }).unwrap();
        partial = partial.place_piece(&p3, Coord { row: 1, col: 0 }).unwrap();
        println!("{:#?}", partial);

        let partial_new = partial.place_piece(&p4, Coord { row: 1, col: 1 });
        println!("{:#?}", partial_new);

        assert!(partial_new.is_some());

        let partial_new = partial_new.unwrap();
        assert_eq!(
            partial_new,
            PartialSolution(vec![vec![p1], vec![p2, p3, p4]])
        );
    }

    #[test]
    fn coord_00() {
        let c = ith_coord(0);
        assert_eq!(0, c.row);
        assert_eq!(0, c.col);
    }

    #[test]
    fn coord_01() {
        let c = ith_coord(1);
        assert_eq!(1, c.row);
        assert_eq!(-1, c.col);
    }

    #[test]
    fn coord_02() {
        let c = ith_coord(2);
        assert_eq!(1, c.row);
        assert_eq!(0, c.col);
    }

    #[test]
    fn coord_03() {
        let c = ith_coord(3);
        assert_eq!(1, c.row);
        assert_eq!(1, c.col);
    }

    #[test]
    fn coord_04() {
        let c = ith_coord(4);
        assert_eq!(2, c.row);
        assert_eq!(-2, c.col);
    }

    use ::*;
    fn p1() -> Piece {
        Piece::new(
            Tab::Out(Shape::Spade),
            Tab::In(Shape::Heart),
            Tab::In(Shape::Diamond),
        )
    }
    fn p1_rot() -> Piece {
        Piece::new(
            Tab::In(Shape::Heart),
            Tab::In(Shape::Diamond),
            Tab::Out(Shape::Spade),
        )
    }
    fn p2() -> Piece {
        Piece::new(
            Tab::Out(Shape::Heart),
            Tab::In(Shape::Diamond),
            Tab::In(Shape::Club),
        )
    }
    fn p3() -> Piece {
        Piece::new(
            Tab::In(Shape::Spade),
            Tab::In(Shape::Heart),
            Tab::Out(Shape::Club),
        )
    }
    fn p4() -> Piece {
        Piece::new(
            Tab::In(Shape::Spade),
            Tab::Out(Shape::Heart),
            Tab::In(Shape::Club),
        )
    }
    fn p5() -> Piece {
        Piece::new(
            Tab::Out(Shape::Heart),
            Tab::Out(Shape::Diamond),
            Tab::In(Shape::Spade),
        )
    }
    fn p6() -> Piece {
        Piece::new(
            Tab::In(Shape::Heart),
            Tab::Out(Shape::Diamond),
            Tab::Out(Shape::Spade),
        )
    }
    fn p7() -> Piece {
        Piece::new(
            Tab::Out(Shape::Spade),
            Tab::In(Shape::Diamond),
            Tab::Out(Shape::Club),
        )
    }
    fn p8() -> Piece {
        Piece::new(
            Tab::Out(Shape::Spade),
            Tab::In(Shape::Diamond),
            Tab::In(Shape::Club),
        )
    }
    fn p9() -> Piece {
        Piece::new(
            Tab::In(Shape::Club),
            Tab::Out(Shape::Diamond),
            Tab::In(Shape::Spade),
        )
    }

    #[test]
    fn rotate_piece() {
        assert_eq!(p1().rot(), p1_rot())
    }

    #[test]
    fn solve_one_piece() {
        let puzzle = vec![p1()];
        let soln = solve(puzzle).unwrap();
        println!("{:#?}", soln);
    }

    #[test]
    fn solve_preordered_4() {
        let puzzle = vec![p1(), p2(), p3(), p4()];
        let soln = solve(puzzle).unwrap();
        println!("{:#?}", soln);

    }

    #[test]
    fn solve_unordered_4_1() {
        let puzzle = vec![p1(), p2(), p4(), p3()];
        let soln = solve(puzzle).unwrap();
        println!("{:#?}", soln);

    }

    #[test]
    fn solve_unordered_4_with_rot() {
        let puzzle = vec![p1_rot(), p2(), p4(), p3()];
        let soln = solve(puzzle).unwrap();
        println!("{:#?}", soln);

    }

    #[test]
    fn solve_preordered_9() {
        let puzzle = vec![p1(), p2(), p3(), p4(), p5(), p6(), p7(), p8(), p9()];
        let soln = solve(puzzle).unwrap();
        println!("{:#?}", soln);

    }

    #[test]
    fn solve_unordered_9_1() {
        let puzzle = vec![p2(), p1(), p5(), p4(), p3(), p6(), p7(), p8(), p9()];
        let soln = solve(puzzle).unwrap();
        println!("{:#?}", soln);

    }

    #[test]
    fn solve_unordered_9_2() {
        let puzzle = vec![p2(), p1(), p5(), p8(), p3(), p6(), p9(), p4(), p7()];
        let soln = solve(puzzle).unwrap();
        println!("{:#?}", soln);

    }
}
